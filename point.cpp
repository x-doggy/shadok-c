#include "point.hpp"

void point_plus(point_t *dest, const point_t *a, const point_t *b) {
  dest->x = a->x + b->x;
  dest->y = a->y + b->y;
}

void point_minus(point_t *dest, const point_t *a, const point_t *b) {
  const point_t minusB = {-b->x, -b->y};
  point_plus(dest, a, &minusB);
}