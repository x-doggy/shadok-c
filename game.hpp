#ifndef _SHADOK_GAME_H_
#define _SHADOK_GAME_H_

#include <cassert>

#include "utils.hpp"
#include "point.hpp"
#include "board.hpp"

#define  _(STR) (STR)
#define N_(STR) (STR)

#define MOVES_NUMBER 40
#define TRANSISTORS_NUMBER 100

#define GIBIES_NUMBER 15
#define FLOWERS_NUMBER 20

#define C_PLAYER_ALREADY_PLACED -1
#define C_PLAYER_PLACED_SUCCESSFUL 1

#define SHADOK_SYMB ('X')
#define GIBY_SYMB ('G')
#define EMPTY_SYMB ('.')

int is_cell_empty(const board_t *board, const point_t *p);
int is_cell_giby(const board_t *board, const point_t *p);
int is_cell_flower(const board_t *board, const point_t *p);
int is_cell_shadok(const board_t *board, const point_t *p);

int is_out_of_border_bounds(const point_t *p);

void generate_random_point(point_t *p);
int place_player_to_empty_position_on_board(const board_t *board, const point_t *p, const char player);
int place_player_to_board_by_random(
    board_t *board,
    const char player,
    point_t *newPoint
);

static void move_player(board_t *board, const point_t *newPosition, point_t *playerP, const char player, point_t **flowers);
static void generate_new_flower(board_t *board, const point_t *newPosition, point_t **flowers);

point_t process_direction(char d[]);
void process_new_position(
    board_t *board,
    point_t *direction,
    point_t *playerP,
    const char player,
    point_t **flowers
);
void process_enemies(board_t *board, point_t **gibies, point_t **flowers);

#endif
