#include "game.hpp"
#include <clocale>

const char *shadok_game_version = "v0.3.0";
const char *program_name;

/* Main board: two-dementional array of characters */
board_t *board;
/* Flowers: objects need to take, array of points */
point_t *flowers[FLOWERS_NUMBER];
/* Gibies: enemies avoid to, array of points */
point_t *gibies[GIBIES_NUMBER];
/* Shadok: main player, position on the board */
point_t *shadok;

/* Score: number of transistors took */
int score = 0;
/* limitation by number of moves */
int steps = MOVES_NUMBER;

static void print_version();
static void usage(int is_err);

int main(int argc, char **argv) {
  
  program_name = argv[0];

  setlocale(LC_ALL, "");
  
  int is_help_option = 0;
  int is_unrecognized_option = 0;
  if (argc > 1) {

    for (int i = 1; i < argc; i++) {
      if ( ! strcmp(argv[i], "-v") || ! strcmp(argv[i], "--version") ) {
        print_version();
      } else if ( ! strcmp(argv[i], "-h") || ! strcmp(argv[i], "--help") ) {
        is_help_option = 1;
      } else {
        is_unrecognized_option = 1;
      }
    }

    usage(is_unrecognized_option != 0 ||
          (is_help_option == 0 && is_unrecognized_option == 0));

    exit(EXIT_SUCCESS);
  }

  srand((unsigned int) time(NULL));

  /* BOARD initialization */
  board = (board_t *) malloc(sizeof(*board) * BOARD_SIZE_VER);
  for (int i = 0; i < BOARD_SIZE_VER; i++) {
    board->board[i] = (char *) malloc(sizeof(char) * BOARD_SIZE_HOR);
  }

  /* Flowers initialization */
  for (int i = 0; i < FLOWERS_NUMBER; i++) {
    flowers[i] = (point_t *) malloc(sizeof(point_t));
  }

  /* Gibies initialization */
  for (int i = 0; i < GIBIES_NUMBER; i++) {
    gibies[i] = (point_t *) malloc(sizeof(point_t));
  }

  /* Shadok initialization */
  shadok = (point_t *) malloc(sizeof(*shadok));

#if 0
  /* Original placement from the book */
  strcpy(board->board[0] , ".......9............");
  strcpy(board->board[1] , ".G.3.....7..G.......");
  strcpy(board->board[2] , "1......5.....G.G..8.");
  strcpy(board->board[3] , "..........4...3.....");
  strcpy(board->board[4] , ".....5..G...G...2..1");
  strcpy(board->board[5] , "..........8...G.....");
  strcpy(board->board[6] , ".9.....G.........1.G");
  strcpy(board->board[7] , "....5........4......");
  strcpy(board->board[8] , ".......7...G...G..G.");
  strcpy(board->board[9] , ".....G..............");
  strcpy(board->board[10], "..5......2...8......");
  strcpy(board->board[11], ".......G........G...");
#else
  /* Use own placement */
  board_clear(board, EMPTY_SYMB);
#endif

  /* Place flowers into the board */
  for (int i = 0; i < FLOWERS_NUMBER; i++) {
    place_player_to_board_by_random(board, (const char) (rand_range(1, 9) + '0'), flowers[i]);
  }

  /* Place gibies into the board */
  for (int i = 0; i < GIBIES_NUMBER; i++) {
    place_player_to_board_by_random(board, GIBY_SYMB, gibies[i]);
  }
  
  /* Place shadok into the board */
  place_player_to_board_by_random(board, SHADOK_SYMB, shadok);

  /* Game main loop */
  char key[3] = {0};
  point_t direction;
  do {
    screen_clear();
    board_print(board);
    printf(_("%2d moves remain\n"), steps--);
    printf(_("%2d transistors took\n"), score);
    printf(_("shadok> "));
    getline_correct(key, 3);
    direction = process_direction(key);
    process_new_position(board, &direction, shadok, SHADOK_SYMB, flowers);
    process_enemies(board, gibies, flowers);
  } while (steps > 0 && score < TRANSISTORS_NUMBER);

  if (steps <= 0) {
    puts(_("SHADOK MURDERED. GAME OVER."));
  } else if (score >= TRANSISTORS_NUMBER) {
    puts(_("YYYYEEEEEBBBBBBOOOOOIIIIIII!!!!!"));
  }
  
  /* Freeing memory under gibies */
  for (int i = 0; i < GIBIES_NUMBER; i++) {
    free(gibies[i]);
  }
  /* Freeing memory under flowers */
  for (int i = 0; i < FLOWERS_NUMBER; i++) {
    free(flowers[i]);
  }
  /* Freeing memory under shadok */
  free(shadok);

  /* Freeing memory under board */
  for (int i = 0; i < BOARD_SIZE_VER; i++) {
    free(board->board[i]);
  }
  free(board);

  return (EXIT_SUCCESS);
}

void print_version() {
  printf("shadok-c (%s)\n", shadok_game_version);
  puts("");
  printf(_("\
Copyright (C) %s, Vladimir Stanik.\n\
License GPLv3+: GNU GPL version 3 or later \
<http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\n"), "2018");
}

void usage(int is_err) {
  printf(_("\
Usage: %s [OPTION]...\n\n"), program_name);
  fputs("", stdout);

  if (is_err != 0) {
    fputs(_("\
Unrecognized option.\n\n"), stderr);
    fputs("", stdout);
  }
  
  fputs(_("\
\"Shadok around gibies\" mini console game.\n\n"), stdout);
  fputs("", stdout);
  
  fputs(_("\
-h, --help          display this help and exit\n\
-v, --version       display version information and exit\n"), stdout);
  fputs("", stdout);
  
  fputs(_("\n\
Control commands (directions where will the shadok steps):\n\
u       = UP\n\
d       = DOWN\n\
l       = LEFT\n\
r       = RIGHT\n\
ul, lu  = UP LEFT\n\
ur, ru  = UP RIGHT\n\
dl, ld  = DOWN LEFT\n\
dr, rd  = DOWN RIGHT\n\
"), stdout);
  fputs("", stdout);
}

